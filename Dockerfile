FROM ruby:2.7.0-alpine

ENV \
  DEV_PACKAGES="bash ruby-dev build-base postgresql-dev tzdata git nodejs yarn" \
  APP_HOME="/app"

WORKDIR $APP_HOME

RUN \
  rm -rf /app && \
  mkdir -p /app/tmp/pids /app/tmp/cache /app/tmp/sockets

# Установка зависимостей
RUN set -ex && \
  apk --update --upgrade add --no-cache $DEV_PACKAGES && \
  rm -rf /var/cache/apk/* && \
  echo ‘gem: --no-document’ > /etc/gemrc && \
  gem install bundler

# Установка часового пояса
RUN cp /usr/share/zoneinfo/Europe/Moscow /etc/localtime && \
    echo "Europe/Moscow" > /etc/timezone

# Установка гемов
COPY Gemfile* ./
RUN bundle install

# Установка пакетов из веб пака
COPY package.json ./
COPY yarn.lock ./
RUN yarn install --check-files
